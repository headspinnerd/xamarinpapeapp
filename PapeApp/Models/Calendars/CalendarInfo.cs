﻿using System;
using System.Collections.Generic;

namespace PapeApp.Models.Calendars
{
    // No need??
    public class CalendarInfo
    {
        public string Kind { get; set; }
        public string Etag { get; set; }
        public string Id { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Timezone { get; set; }
        public string SummaryOverride { get; set; }
        public string ColorId { get; set; }
        public string BackgroundColor { get; set; }
        public string ForegroundColor { get; set; }
        public bool Hidden { get; set; }
        public bool Selected { get; set; }
        public string AccessRole { get; set; }
        public List<DefaultReminder> DefaultReminders {get;set;}
    }
}

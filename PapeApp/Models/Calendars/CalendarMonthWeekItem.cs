﻿using System;
using System.Collections.Generic;
using System.Linq;
using PapeApp.Utility;

namespace PapeApp.Models.Calendars
{
    public class CalendarMonthWeekItem
    {
        public DateTimeOffset FirstDayOfWeek { get; set; }
        public List<List<CalendarItem>> DayOfWeekItems { get; set; }
        public List<CalendarMonthMultiDayItem> MultiDayItems { get; set; }
            = new List<CalendarMonthMultiDayItem>();

        public CalendarMonthWeekItem(int week, DateTimeOffset firstCalDayOfMonth, List<Item> items)
        {
            FirstDayOfWeek = firstCalDayOfMonth.AddDays(7 * week);

            DayOfWeekItems = Enumerable.Range(0, 7).Select(n =>
            {
                var beginTimeOfDay = FirstDayOfWeek.AddDays(n);
                var endTimeOfDay = beginTimeOfDay.AddDays(1).AddMinutes(-1);
                var calendarItems = items
                    .Where(i => i.ContainsRangeDate(beginTimeOfDay, endTimeOfDay))
                    .Select(i =>
                    {
                        if (i.CreateMultiDayEvent(FirstDayOfWeek) is CalendarMonthMultiDayItem multiDayItem)
                        {
                            // 複日イベントは横長のUILabelを追加する。
                            if (!MultiDayItems.Any(mItem => mItem.EventId == i.Id))
                            // 重複を排除する
                            {
                                MultiDayItems.Add(multiDayItem);
                            }
                            return CalendarItem.Dummy;
                        }
                        return new CalendarItem(i.Id, i.Summary, i.Start, i.End, 
                            i.Reminders, i.Attendees, i.Description);
                    }).ToList();
                return calendarItems;
            }).ToList();
        }
    }

    public class CalendarMonthMultiDayItem
    {
        public CalendarMonthMultiDayItem(Item item, DayOfWeek startDOW, DayOfWeek endDOW)
        {
            Item = item;
            EventId = Item.Id;
            StartDOW = startDOW;
            EndDOW = endDOW;
        }

        public string EventId { get; }
        public Item Item { get; }
        public DayOfWeek StartDOW { get; }
        public DayOfWeek EndDOW { get; }
    }
}

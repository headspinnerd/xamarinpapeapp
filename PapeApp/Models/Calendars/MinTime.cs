﻿using System;

namespace PapeApp.Models.Calendars
{
    public enum MinTime
    {
        OneYearAgo,
        HalfYearAgo,
        QuarterYearAgo,
        OneMonthAgo
    }
}

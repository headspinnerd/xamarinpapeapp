﻿using System;
using System.Collections.Generic;

namespace PapeApp.Models.Calendars
{
    public class CalendarItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public DateTimes Start { get; set; }
        public DateTimes End { get; set; }
        public bool IsDummy { get; } = false;
        public bool IsAllDay { get; } = false;
        public Reminders Reminders { get; set; }
        public List<Attendee> Attendees { get; set; }
        public string Memo { get; set; }

        public CalendarItem(string id,
            string title, DateTimes start, DateTimes end,
            Reminders reminders, List<Attendee> attendees, string memo)
        {
            Id = id;
            Title = title;
            Start = start;
            End = end;
            IsAllDay = start.DateTime == null && end.DateTime == null;
            Reminders = reminders;
            Memo = memo;
            Attendees = attendees;
        }

        public CalendarItem(bool isDummy)
        {
            IsDummy = isDummy;
        }

        /// <summary>
        /// 複日イベントは横長のUILabelを上から被せるため、
        /// Cellごとのボタンはダミーにする。
        /// </summary>
        public static CalendarItem Dummy => new CalendarItem(true);
    }
}

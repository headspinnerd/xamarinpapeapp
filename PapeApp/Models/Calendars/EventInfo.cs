﻿using System;
using System.Collections.Generic;

namespace PapeApp.Models.Calendars
{
    // 以下サイトで自動生成
    // http://json2csharp.com/
    public class EventInfo
    {
        public string Kind { get; set; }
        public string Etag { get; set; }
        public string Summary { get; set; }
        public DateTime Updated { get; set; }
        public string Timezone { get; set; }
        public string AccessRole { get; set; }
        public List<DefaultReminder> DefaultReminders { get; set; }
        public string NextPageToken { get; set; }
        public List<Item> Items { get; set; }
    }

    public class DefaultReminder
    {
        public string Method { get; set; }
        public int Minutes { get; set; }

        public static string Popup => "popup";
        public static string Email => "email";
    }

    public class Creator
    {
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public bool Self { get; set; }
    }

    public class Organizer
    {
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public bool Self { get; set; }
    }

    public class DateTimes
    {
        public string Date { get; set; }
        public DateTime? DateTime { get; set; }
        public string TimeZone { get; set; }
    }

    public class Override
    {
        public string Method { get; set; }
        public int Minutes { get; set; }
    }

    public class Reminders
    {
        public bool UseDefault { get; set; }
        public List<Override> Overrides { get; set; }
    }

    public class OriginalStartTime
    {
        public DateTime DateTime { get; set; }
        public string Date { get; set; }
    }

    public class Attendee
    {
        public string Email { get; set; }
        public bool Self { get; set; }
        public string ResponseStatus { get; set; }
        public string DisplayName { get; set; }
        public bool? Organizer { get; set; }
    }

    public class Source
    {
        public string Url { get; set; }
        public string Title { get; set; }
    }

    public class Shared
    {
        public string OriginalTitle { get; set; }
    }

    public class ExtendedProperties
    {
        public Shared Shared { get; set; }
    }

    public class Item
    {
        public string Kind { get; set; }
        public string Etag { get; set; }
        public string Id { get; set; }
        public string Status { get; set; }
        public string HtmlLink { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string Summary { get; set; }
        public Creator Creator { get; set; }
        public Organizer Organizer { get; set; }
        public DateTimes Start { get; set; }
        public DateTimes End { get; set; }
        public string Transparency { get; set; }
        public string ICalUID { get; set; }
        public int Sequence { get; set; }
        public Reminders Reminders { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public List<string> Recurrence { get; set; }
        public string RecurringEventId { get; set; }
        public OriginalStartTime OriginalStartTime { get; set; }
        public string Visibility { get; set; }
        public List<Attendee> Attendees { get; set; }
        public bool? GuestsCanInviteOthers { get; set; }
        public bool? PrivateCopy { get; set; }
        public Source Source { get; set; }
        public ExtendedProperties ExtendedProperties { get; set; }
    }
}

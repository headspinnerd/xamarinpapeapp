﻿using System;
using System.Collections.Generic;
using System.Linq;
using PapeApp.Utility;

namespace PapeApp.Models.Calendars
{
    public static class CalendarLogic
    {
        public static CalendarMonthCollection CreateMonthList(
            this List<Item> items, DateTimeOffset currentDay)
        {
            var firstDayOfMonth = currentDay.ConvertToStartOfMonth();
            var firstCalDayOfMonth = firstDayOfMonth
                .AddDays(-(double)firstDayOfMonth.DayOfWeek);
            var numberOfWeeks = GetNumberOfWeeksInMonth(firstCalDayOfMonth, firstDayOfMonth);
            var weekItems = Enumerable.Range(0, numberOfWeeks)
                .Select(w => new CalendarMonthWeekItem(w, firstCalDayOfMonth, items)).ToList();
            var collection = new CalendarMonthCollection(firstCalDayOfMonth, numberOfWeeks, weekItems);
            return collection;
        }

        private static int GetNumberOfWeeksInMonth(DateTimeOffset firstCalDayOfMonth, 
            DateTimeOffset firstDayOfMonth)
        {
            var necessaryDaysInMonth = (int)firstDayOfMonth.DayOfWeek
                + firstDayOfMonth.AddMonths(1).AddDays(-1).Day;
            return (necessaryDaysInMonth - 1) / 7 + 1;
        }
    }
}

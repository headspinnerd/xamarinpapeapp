﻿using System;
using System.Collections.Generic;

namespace PapeApp.Models.Calendars
{
    public class CalendarMonthCollection
    {
        public CalendarMonthCollection(DateTimeOffset firstCalDayOfMonth, int numberOfWeeks, List<CalendarMonthWeekItem> weekItems)
        {
            FirstCalDayOfMonth = firstCalDayOfMonth;
            NumberOfWeeks = numberOfWeeks;
            WeekItems = weekItems;
        }

        public DateTimeOffset FirstCalDayOfMonth { get; set; }
        public int NumberOfWeeks { get; set; }
        public List<CalendarMonthWeekItem> WeekItems { get; set; }
    }
}

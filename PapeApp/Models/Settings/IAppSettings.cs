﻿using System;
namespace PapeApp.Models.Settings
{
    public interface IAppSettings
    {
        string SpouseMailAddress { get; set; }
        string SpouseName { get; set; }
    }
}

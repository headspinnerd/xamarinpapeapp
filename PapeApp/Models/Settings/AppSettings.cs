﻿using System;
using GalaSoft.MvvmLight.Ioc;
using PapeApp.Services;

namespace PapeApp.Models.Settings
{
    public class AppSettings: IAppSettings
    {
        private IPreferenceService Preference
            => SimpleIoc.Default.GetInstance<IPreferenceService>();

        public string SpouseMailAddress
        {
            get
            {
                return Preference.GetValue<string>(UserDefaultsKey.SpouseMailAddress);
            }
            set
            {
                Preference.SetValue<string>(value, UserDefaultsKey.SpouseMailAddress);
            }
        }

        public string SpouseName
        {
            get
            {
                return Preference.GetValue<string>(UserDefaultsKey.SpouseName);
            }
            set
            {
                Preference.SetValue<string>(value, UserDefaultsKey.SpouseName);
            }
        }
    }
}

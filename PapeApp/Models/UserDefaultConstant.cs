﻿using System;
namespace PapeApp.Models
{
    public enum UserDefaultsKey
    {
        CalendarMaxResults,
        TimeMax,
        TimeMin,
        TestWithoutInternet,
        SpouseMailAddress,
        SpouseName
    }
}

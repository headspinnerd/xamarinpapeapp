﻿using System;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using PapeApp.Models.Calendars;
using PapeApp.Services;

namespace PapeApp.Utility
{
    public static class ReminderExtension
    {
        public static IGoogleCalendarService GoogleService
            => SimpleIoc.Default.GetInstance<IGoogleCalendarService>();

        public static bool HasReminder(this Reminders reminders)
        {
            return reminders.UseDefault || reminders.Overrides.Any();
        }

        public static int FirstReminderMinutes(this Reminders reminders)
        {
            var reminderDefaults = GoogleService.DefaultReminders;

            if (reminders.UseDefault)
            {
                return reminderDefaults.First().Minutes;
            }
            else
            {
                if (reminders.Overrides.Any())
                {
                    return reminders.Overrides.First().Minutes;
                }
                else
                {
                    return -1;
                }
            }
        }
    }
}

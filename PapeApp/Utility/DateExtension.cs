﻿using System;
using GalaSoft.MvvmLight.Ioc;
using PapeApp.Models;
using PapeApp.Models.Calendars;
using PapeApp.Services;

namespace PapeApp.Utility
{
    public static class DateExtension
    {
        static IPreferenceService PreferenceService 
            => SimpleIoc.Default.GetInstance<IPreferenceService>();

        public static DateTimeOffset ApplyMinTime(this DateTimeOffset now)
        {
            var minTime = PreferenceService.GetValue<MinTime>(
                UserDefaultsKey.TimeMin, MinTime.QuarterYearAgo);

            switch (minTime)
            {
                case MinTime.OneYearAgo:
                    return now.AddYears(-1);
                case MinTime.HalfYearAgo:
                    return now.AddMonths(-6);
                case MinTime.QuarterYearAgo:
                    return now.AddMonths(-3);
                case MinTime.OneMonthAgo:
                    return now.AddMonths(-1);
                default:
                    Log.Instance.Warn("unexpected MinTime");
                    return now;
            }
        }

        public static DateTimeOffset ConvertToStartOfMonth(this DateTimeOffset date)
        {
            return new DateTimeOffset(
                date.Year,
                date.Month,
                1, 0, 0, 0, date.Offset);
        }

        public static DateTimeOffset ConvertToStartOfDay(this DateTimeOffset date)
        {
            return new DateTimeOffset(
                date.Year,
                date.Month,
                date.Day,
                0, 0, 0, date.Offset);
        }

        public static bool ContainsRangeDate(this Item item,
            DateTimeOffset beginTimeOfDay, DateTimeOffset endTimeOfDay)
        {
            return item.FetchStartDate() < endTimeOfDay
                    && item.FetchEndDate() > beginTimeOfDay;
        }

        public static CalendarMonthMultiDayItem CreateMultiDayEvent(this Item item,
            DateTimeOffset firstDayOfWeek)
        {
            var startOfEvent = item.FetchStartDate();
            var startOfEventInWeek = new DateTimeOffset(
                Math.Max(firstDayOfWeek.Ticks, startOfEvent.Ticks), startOfEvent.Offset);
            var convertedStartDate = startOfEventInWeek.AddDays(1).ConvertToStartOfDay();
            var endDayOfWeek = firstDayOfWeek.AddDays(7).AddMinutes(-1);
            var endOfEvent = item.FetchEndDate();
            var endOfEventInWeek = new DateTimeOffset(
                Math.Min(endDayOfWeek.Ticks, endOfEvent.Ticks), endOfEvent.Offset);
            if (convertedStartDate < endOfEventInWeek)
                return new CalendarMonthMultiDayItem(item, 
                    startOfEvent.DayOfWeek, endOfEventInWeek.DayOfWeek);
            return null;
        }

        public static DateTimeOffset FetchStartDate(this Item item)
        {
            return FetchDate(item.Start.Date, item.Start.DateTime, item.Id);
        }

        public static DateTimeOffset FetchEndDate(this Item item)
        {
            return FetchDate(item.End.Date, item.End.DateTime, item.Id);
        }

        public static string ConvertToDateHeaderLbl(this DateTimeOffset currentDate, int selectedId)
        {
            switch (selectedId)
            {
                case 0: // Month
                    return currentDate.ToString("yyyy/MM");
                case 1: // List
                    break;
                case 2: // Day
                    break;
                case 3: // Week
                    break;
            }
            return "";
        }

        public static string ConvertToTimeLabel(this DateTimes startDate, 
            DateTimes endDate, bool isAllDay)
        {
            var startDateTime = FetchDate(startDate.Date, startDate.DateTime, "Unknown");
            var endDateTime = FetchDate(endDate.Date, endDate.DateTime, "Unknown");
            if (isAllDay)
                return startDateTime.ToString("MM/dd");
            var isSameDay = startDateTime.ConvertToStartOfDay() == endDateTime.ConvertToStartOfDay();
            if (isSameDay)
            {
                return startDateTime.ToString("HH:mm") + "-" + endDateTime.ToString("HH:mm");
            }
            else
            {
                return startDateTime.ToString("MM/dd HH:mm") 
                    + "-" + endDateTime.ToString("MM/dd HH:mm");
            }
        }

        public static string ConvertToStartLabel(this DateTimes startDate, bool isAllDay)
        {
            var startDateTime = FetchDate(startDate.Date, startDate.DateTime, "Unknown");
            if (isAllDay)
                return startDateTime.ToString("MM/dd");
            return startDateTime.ToString("MM/dd HH:mm");
        }

        public static string ConvertToEndLabel(this DateTimes endDate, bool isAllDay)
        {
            var endDateTime = FetchDate(endDate.Date, endDate.DateTime, "Unknown");
            if (isAllDay)
                return endDateTime.AddDays(-1).ToString("MM/dd");
            return endDateTime.ToString("MM/dd HH:mm");
        }

        private static DateTimeOffset FetchDate(string date, DateTime? dateTime, string id)
        {
            if (date == null && dateTime == null)
            {
                Log.Instance.Error($"Both Date is null!! Item Id: {id}");
                throw new Exception("Both Date is null");
            }

            if (dateTime is DateTime _dateTime)
            {
                return new DateTimeOffset(_dateTime);
            }
            DateTime oDate = DateTime.MinValue;
            try
            {
                oDate = DateTime.Parse(date);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex.Message);
            }
            return oDate;
        }
    }
}

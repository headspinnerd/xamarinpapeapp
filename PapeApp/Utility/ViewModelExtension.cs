﻿using System;
using PapeApp.ViewModels;

namespace PapeApp.Utility
{
    public static class ViewModelExtension
    {
        public static string ToViewModeKey(this BaseViewModel viewModel)
            => nameof(viewModel).Replace("Model", "");
    }
}

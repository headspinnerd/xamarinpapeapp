﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using NLog;

namespace PapeApp.Services
{
    public static class Log
    {
        static ILogger _Instance;
        public static ILogger Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = SimpleIoc.Default.GetInstance<ILogger>();
                }
                return _Instance;
            }
        }
    }
}
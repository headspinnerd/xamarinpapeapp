﻿using System;
using GalaSoft.MvvmLight.Views;
using PapeApp.Models.Calendars;

namespace PapeApp.Services
{
    public interface IExtendedNavigationService : INavigationService
    {
        void NavigateTo(string pageKey, NavigationType navigationType, object parameter = null);

        void Dismiss();
    }
}

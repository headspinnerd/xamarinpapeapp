﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PapeApp.Models.Calendars;
using Reactive.Bindings;

namespace PapeApp.Services
{
    public interface IGoogleCalendarService
    {
        List<DefaultReminder> DefaultReminders { get; set; }

        Task<EventInfo> GetEvents();

        ReactiveProperty<DateTimeOffset> CurrentDate { get; set; }
    }
}

﻿using System;
namespace PapeApp.Services
{
    public interface IGoogleSignInService
    {
        string MailAddress { get; set; }
        string AccessToken { get; set; }
        void SignIn();
        void SignOut();
        void Disconnect();
    }
    //com.googleusercontent.apps.438491656628-9g92gqrei90haip6jhe2am6g0tpj2evv
}

﻿using System;
using PapeApp.Models;

namespace PapeApp.Services
{
    public interface IPreferenceService
    {
        void SetValue<T>(T value, UserDefaultsKey key);

        T GetValue<T>(UserDefaultsKey key, T defaults = default);

        void RemoveValue(UserDefaultsKey key);

        bool ContainValue(UserDefaultsKey key);
    }
}

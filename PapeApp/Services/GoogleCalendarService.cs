﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Newtonsoft.Json;
using PapeApp.Models;
using PapeApp.Models.Calendars;
using PapeApp.Utility;
using Reactive.Bindings;

namespace PapeApp.Services
{
    public class GoogleCalendarService: IGoogleCalendarService
    {
        private IGoogleSignInService GoogleSignInService
            => SimpleIoc.Default.GetInstance<IGoogleSignInService>();
        private static IPreferenceService PreferenceService
            => SimpleIoc.Default.GetInstance<IPreferenceService>();
        private static IDialogService DialogService => 
            SimpleIoc.Default.GetInstance<IDialogService>();

        public ReactiveProperty<DateTimeOffset> CurrentDate { get; set; }
            = new ReactiveProperty<DateTimeOffset>(DateTimeOffset.Now);

        public List<DefaultReminder> DefaultReminders { get; set; }

        private const string GoogleCalendarUrlHome = "https://www.googleapis.com/calendar/v3";

        public async Task<EventInfo> GetEvents()
        {
            string email = GoogleSignInService.MailAddress;
            var results = await GetData<EventInfo>(GetCalendarEventUrl(email) +
                                GetApiKeyUrl() +
                                GetParameter());
            DefaultReminders = results.DefaultReminders;
            return results;
        }

        public async Task<T> GetData<T>(string requestUrl) where T: class
        {
            string token = GoogleSignInService.AccessToken;

            //カレンダーを取得します。
            string result = String.Empty;
            try
            {
                //URLを設定
                string url = GoogleCalendarUrlHome + requestUrl;

                /*******************************************************************/
                //①HttpClientを用いた接続例
                /*******************************************************************/
                var client = new System.Net.Http.HttpClient();
                //HTTPヘッダーに取得したトークンを設定
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                //URLを設定
                result = await client.GetStringAsync(url);

                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (System.Net.WebException webEx)
            {
                await DialogService.ShowError(webEx.Message, "Error", "OK", null);
                return null;
            }
            catch (System.Net.Http.HttpRequestException)
            {
                //サインインしていないと認証エラーが発生する場合がある
                return null; //hre.Message;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + System.Environment.NewLine + ex.StackTrace);
                return null; //ex.Message;
            }
        }

        //URLは以下を参照
        //https://developers.google.com/google-apps/calendar/v3/reference/
        //https://developers.google.com/calendar/v3/reference/events/list

        //カレンダーを取得するURL
        string GetCalendarUrl(string accountId)
        {
            const string url = "/users/me/calendarList/{0}";
            return String.Format(url, accountId);
        }
        //イベントを取得するURL
        string GetCalendarEventUrl(string accountId)
        {
            const string url = "/calendars/{0}/events";
            return String.Format(url, accountId);
        }
        //URLにAPI_KEYを設定する
        private static string GetApiKeyUrl()
        {
            //if (Device.RuntimePlatform == Device.iOS)
            //{
                //Google API Console でWebアプリケーションに設定したAPI_KEYをここに貼り付ける
                return "?key=AIzaSyBFnabiJq0mGTmMqBc4RlxbMdFvk3OWKRc";
            //}
            //else if (Device.RuntimePlatform == Device.Android)
            //{
            //    //Google API Console でWebアプリケーションに設定したAPI_KEYをここに貼り付ける
            //    return "?key=AIzaSyxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            //}
            //return String.Empty;
        }

        private static string GetParameter()
        {
            // 1回のFetchで取得できるイベント数の上限(デフォルトは1000)
            var maxResults = PreferenceService.GetValue<int>(
                UserDefaultsKey.CalendarMaxResults, 1000);

            // Fetch開始時間、終了時間
            string format = "yyyy-MM-ddTHH:mm:ssZ";　//"2019-04-01T23:59:59Z";
            var timeMin = DateTimeOffset.Now.ApplyMinTime().ToString(format);
            var timeMax = DateTimeOffset.Now.AddYears(1).ToString(format);

            // 繰り返しイベントの１つ１つをSingleEventとして扱うかどうか
            var singleEvents = "true";

            return $"&maxResults={maxResults}" +
                $"&timeMin={timeMin}" +
                $"&timeMax={timeMax}" +
                $"&singleEvents={singleEvents}";
        }
    }
}

﻿using System;
using PapeApp.Services;
using Reactive.Bindings;

namespace PapeApp.ViewModels
{
    public class CalendarContainerViewModel: BaseViewModel
    {
        public static string ViewModelKey = "CalendarContainerView";

        public ReactiveCommand DecideCommand = new ReactiveCommand();

        public CalendarContainerViewModel()
        {
            DecideCommand.Subscribe(_ => Log.Instance.Debug("calendar container test"));
        }

        public override void OnLoad(object parameter)
        {
            base.OnLoad(parameter);
        }

        public override void OnAppear()
        {
            base.OnAppear();
        }

        public override void OnDisappear()
        {
            base.OnDisappear();
        }
    }
}

﻿using System;
using PapeApp.Services;

namespace PapeApp.ViewModels
{
    public class CalendarListViewModel : BaseViewModel
    {
        public static string ViewModelKey = "CalendarListView";

        public CalendarListViewModel()
        {
        }

        public override void OnLoad(object parameter)
        {
            base.OnLoad(null);
            Log.Instance.Debug("test debug list");
        }
    }
}

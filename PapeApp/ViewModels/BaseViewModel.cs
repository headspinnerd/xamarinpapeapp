﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using PapeApp.Services;

namespace PapeApp.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        IExtendedNavigationService _NavigationService = null;
        public IExtendedNavigationService NavigationService
            => GetAndSetContainer<IExtendedNavigationService>(_NavigationService);

        IGoogleSignInService _GoogleSignInService = null;
        public IGoogleSignInService GoogleSignInService
            => GetAndSetContainer<IGoogleSignInService>(_GoogleSignInService);

        protected List<IDisposable> DisposablePools = new List<IDisposable>();

        private T GetAndSetContainer<T>(T service) where T : class
        {
            if (service == null)
            {
                service = SimpleIoc.Default.GetInstance<T>() as T;
            }
            return service;
        }

        public BaseViewModel()
        {
        }

        public virtual void OnLoad(object parameter)
        {

        }

        public virtual void OnAppear()
        {
        }

        public virtual void OnDisappear()
        {
        }
    }
}

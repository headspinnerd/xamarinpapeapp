﻿using GalaSoft.MvvmLight.Ioc;
using PapeApp.Services;
using Reactive.Bindings;
using System;
using System.Linq;

namespace PapeApp.ViewModels
{
    public class CalendarViewModel : BaseViewModel
    {
        public static string ViewModelKey = "CalendarView";

        public IGoogleCalendarService GoogleCalendarService 
            => SimpleIoc.Default.GetInstance<IGoogleCalendarService>();

        public ReactiveCommand BackCommand = new ReactiveCommand();

        private const string GoogleCalendarUrlHome = "https://www.googleapis.com/calendar/v3";

        public CalendarViewModel()
        {
            BackCommand.Subscribe(_ => NavigationService.GoBack());
        }
    }
}
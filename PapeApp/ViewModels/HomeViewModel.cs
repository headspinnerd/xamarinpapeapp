﻿using NLog;
using Reactive.Bindings;
using System;

namespace PapeApp.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        public static string ViewModelKey = "HomeView";

        public ReactiveProperty<int> NumberOfTapped { get; set; } = new ReactiveProperty<int>();
        public ReactiveProperty<string> TestLabelString { get; set; } = new ReactiveProperty<string>();
        public ReactiveCommand CalendarCommand = new ReactiveCommand();

        public ILogger Logger { get; set; }

        public HomeViewModel()
        {
            //NumberOfTapped.Subscribe( _ => TestLabelString.Value = NumberOfTapped.Value.ToString() );
            CalendarCommand.Subscribe(_ => NavigationService.NavigateTo(CalendarViewModel.ViewModelKey));
            GoogleSignInService.SignIn();

        }

        public override void OnLoad(object parameter)
        {
            base.OnLoad(parameter);
        }
    }
}
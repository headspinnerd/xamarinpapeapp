﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using PapeApp.Models;
using PapeApp.Models.Calendars;
using PapeApp.Services;
using Reactive.Bindings;
using Reactive.Bindings.Extensions;

namespace PapeApp.ViewModels
{
    public class CalendarMonthViewModel : BaseViewModel
    {
        public static string ViewModelKey = "CalendarMonthView";

        public ReactiveProperty<CalendarMonthCollection> MonthDayCollection
            = new ReactiveProperty<CalendarMonthCollection>();

        public ReactiveCommand SwipeLeft = new ReactiveCommand();
        public ReactiveCommand SwipeRight = new ReactiveCommand();
        public ReactiveCommand<CalendarItem> DetailCommand 
            = new ReactiveCommand<CalendarItem>();

        public IGoogleCalendarService GoogleCalendarService 
            => SimpleIoc.Default.GetInstance<IGoogleCalendarService>();

        public CalendarMonthViewModel()
        {
            MonthDayCollection.Value = new List<Item>().CreateMonthList(
                GoogleCalendarService.CurrentDate.Value);

        }

        public override async void OnLoad(object parameter)
        {
            await FetchCalendars();

            SwipeLeft.Subscribe(async _ =>
            {
                GoogleCalendarService.CurrentDate.Value
                = GoogleCalendarService.CurrentDate.Value.AddMonths(1);
                await FetchCalendars();
            }).AddTo(DisposablePools);

            SwipeRight.Subscribe(async _ =>
            {
                GoogleCalendarService.CurrentDate.Value
                = GoogleCalendarService.CurrentDate.Value.AddMonths(-1);
                await FetchCalendars();
            }).AddTo(DisposablePools);

            DetailCommand.Subscribe(item => 
                NavigationService.NavigateTo(CalendarEditViewModel.ViewModelKey, NavigationType.Modal, item));

            base.OnLoad(parameter);
        }

        public override void OnDisappear()
        {
            base.OnDisappear();

            DisposablePools.ForEach(p => p?.Dispose());
        }

        private async Task FetchCalendars()
        {
            var events = await GoogleCalendarService.GetEvents();

            if (events == null)
                // Network Error等の場合nullになる
                return;

            MonthDayCollection.Value = events.Items.CreateMonthList(
                GoogleCalendarService.CurrentDate.Value);
        }
    }
}

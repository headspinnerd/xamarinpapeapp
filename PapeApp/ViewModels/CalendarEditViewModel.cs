﻿using System;
using PapeApp.Models.Calendars;
using Reactive.Bindings;
using PapeApp.Utility;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using PapeApp.Models.Settings;

namespace PapeApp.ViewModels
{
    public class CalendarEditViewModel : BaseViewModel
    {
        public static string ViewModelKey = "CalendarEditView";
        public ReactiveCommand CancelCommand = new ReactiveCommand();
        public IAppSettings AppSettings => SimpleIoc.Default.GetInstance<IAppSettings>();

        public string Title;
        public bool IsAllDay;
        public string StartDate;
        public string EndDate;
        public string SpouseName;
        public bool IsSpouseAccompanied;
        public string SpouseStatus;
        public string Memo;
        public ReactiveProperty<Reminders> Reminders = new ReactiveProperty<Reminders>();
        public ReactiveProperty<bool> AlertSwitchOn = new ReactiveProperty<bool>();
        public ReactiveProperty<int> AlertTime = new ReactiveProperty<int>();
        public ReactiveProperty<string> Unit = new ReactiveProperty<string>();


        public CalendarEditViewModel()
        {
            CancelCommand.Subscribe(_ => NavigationService.Dismiss());
        }

        public override void OnLoad(object parameter)
        {
            base.OnLoad(null);

            if (parameter is CalendarItem item)
            {
                Title = item.Title;
                IsAllDay = item.IsAllDay;
                StartDate = item.Start.ConvertToStartLabel(IsAllDay);
                EndDate = item.End.ConvertToEndLabel(IsAllDay);
                SpouseName = AppSettings.SpouseName;
                IsSpouseAccompanied = item.Attendees?.Any(att => 
                    att.Email == AppSettings.SpouseMailAddress) ?? false;
                SpouseStatus = item.Attendees?.FirstOrDefault(att =>
                    att.Email == AppSettings.SpouseMailAddress)?.ResponseStatus ?? "";
                Memo = item.Memo;
                Reminders.Value = item.Reminders;
                AlertSwitchOn.Value = item.Reminders.HasReminder();
                AlertTime.Value = item.Reminders.FirstReminderMinutes();
            }
        }
    }
}

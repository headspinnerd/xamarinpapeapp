﻿using System;
using Foundation;
using Google.SignIn;
using PapeApp.Services;
using UIKit;

namespace PapeApp.iOS.Services
{
    public class GoogleSignInService: IGoogleSignInService
    {
        public static ISignInUIDelegate SignInUIDelegate = new GoogleSignInUIDelegate();

        public string MailAddress { get; set; }

        public string AccessToken { get; set; }

        public void SignIn()
        {
            Google.SignIn.SignIn.SharedInstance.UIDelegate = SignInUIDelegate;

            var googleServiceDictionary = NSDictionary.FromFile("GoogleService-Info.plist");
            Google.SignIn.SignIn.SharedInstance.ClientID = googleServiceDictionary["CLIENT_ID"].ToString();

            Google.SignIn.SignIn.SharedInstance.SignedIn += (sender, e) => {
                // ここにログイン後の処理を記述します。
                if (e.User != null && e.Error == null)
                {
                    //ここにログイン成功時の処理を記述してください
                    MailAddress = e.User.Profile.Email;
                    AccessToken = e.User.Authentication.AccessToken;
                    System.Diagnostics.Debug.WriteLine("Signed in user: {0}", e.User.Profile.Name);
                }
                else
                {
                    Console.WriteLine(e.Error.Description);
                }
            };

            Google.SignIn.SignIn.SharedInstance.Disconnected += (sender, e) => {
                //ここに切断時の処理を記述してください
                System.Diagnostics.Debug.WriteLine("Disconnected user");
            };


            Google.SignIn.SignIn.SharedInstance.Scopes 
                = new string[] { "https://www.googleapis.com/auth/calendar" };

            //自動サイレントログイン
            //Google.SignIn.SignIn.SharedInstance.SignInUserSilently();

            var user = Google.SignIn.SignIn.SharedInstance.CurrentUser;

            //手動ログイン
            Google.SignIn.SignIn.SharedInstance.SignInUser();
        }

        public void SignOut()
        {
            Google.SignIn.SignIn.SharedInstance.SignOutUser();
        }

        public void Disconnect()
        {
            Google.SignIn.SignIn.SharedInstance.DisconnectUser();
        }
    }

    public class GoogleSignInUIDelegate : SignInUIDelegate
    {
        public override void WillDispatch(SignIn signIn, NSError error)
        {
        }
        public override void PresentViewController(SignIn signIn, UIViewController viewController)
        {
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(viewController, true, null);

        }

        public override void DismissViewController(SignIn signIn, UIViewController viewController)
        {
            UIApplication.SharedApplication.KeyWindow.RootViewController.DismissViewController(true, null);
        }
    }
}

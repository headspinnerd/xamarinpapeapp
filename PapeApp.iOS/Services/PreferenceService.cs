﻿using System;
using Foundation;
using PapeApp.Models;
using PapeApp.Services;

namespace PapeApp.iOS.Services
{
    public class PreferenceService: IPreferenceService
    {
        private NSUserDefaults UserDefaults;

        public PreferenceService()
        {
            UserDefaults = NSUserDefaults.StandardUserDefaults;
        }

        public void SetValue<T>(T value, UserDefaultsKey key)
        {
            if (value is string strValue)
            {
                UserDefaults.SetString(strValue, key.ToString());
            }
            else if (value is int intValue)
            {
                UserDefaults.SetInt(intValue, key.ToString());
            }
            else
            {
                throw new Exception($"Not implemented {typeof(T)}");
            }

            UserDefaults.Synchronize();
        }

        public T GetValue<T>(UserDefaultsKey key, T defaults = default)
        {
            var type = typeof(T);
            if (type == typeof(string))
            {
                string result = UserDefaults.StringForKey(key.ToString());
                if (result == default)
                    return defaults;
                else
                    return (T)Convert.ChangeType(result, typeof(T));
            }
            else if (type == typeof(int))
            {
                nint result = UserDefaults.IntForKey(key.ToString());
                if (result == default)
                    return defaults;
                else
                    return (T)Convert.ChangeType(result, typeof(T));
            }
            else if(defaults is Enum)
            {
                try
                {
                    nint result = UserDefaults.IntForKey(key.ToString());
                    if (result == default)
                        return defaults;
                    else
                        return (T)Convert.ChangeType(result, typeof(T));
                }
                catch(Exception ex)
                {
                    Log.Instance.Error(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                throw new Exception($"Not implemented {typeof(T)}");
            }
        }

        public void RemoveValue(UserDefaultsKey key)
        {
            UserDefaults.RemoveObject(key.ToString());
        }

        public bool ContainValue(UserDefaultsKey key)
        {
            return UserDefaults.ValueForKey(new NSString(key.ToString())) != null;
        }
    }
}

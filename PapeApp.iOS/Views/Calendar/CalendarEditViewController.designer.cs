// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace PapeApp.iOS
{
    [Register ("CalendarEditViewController")]
    partial class CalendarEditViewController
    {
        [Outlet]
        UIKit.UISwitch AlertSwitch { get; set; }


        [Outlet]
        UIKit.UITextField AlertTextField { get; set; }


        [Outlet]
        UIKit.UISwitch AllDaySwitch { get; set; }


        [Outlet]
        PapeApp.iOS.Views.CommonLabel BeforeLabel { get; set; }


        [Outlet]
        UIKit.UIButton CancelButton { get; set; }


        [Outlet]
        UIKit.UIView ContainerView { get; set; }


        [Outlet]
        UIKit.UIButton DeleteButton { get; set; }


        [Outlet]
        UIKit.UITextView EndView { get; set; }


        [Outlet]
        PapeApp.iOS.Views.CommonTextView MemoView { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint MemoViewHeight { get; set; }


        [Outlet]
        UIKit.UILabel SpouseLabel { get; set; }


        [Outlet]
        UIKit.UILabel SpouseStatus { get; set; }


        [Outlet]
        UIKit.UISwitch SpouseSwitch { get; set; }


        [Outlet]
        UIKit.UITextView StartView { get; set; }


        [Outlet]
        UIKit.UITextField TitleField { get; set; }


        [Outlet]
        PapeApp.iOS.Views.CommonLabel UnitLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
        }
    }
}
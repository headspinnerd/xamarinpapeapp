// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace PapeApp.iOS.Views
{
    [Register ("MonthCollectionCell")]
    partial class MonthCollectionCell
    {
        [Outlet]
        UIKit.UIButton BtnPlan1 { get; set; }


        [Outlet]
        UIKit.UIButton BtnPlan2 { get; set; }


        [Outlet]
        UIKit.UIButton BtnPlan3 { get; set; }


        [Outlet]
        UIKit.UIButton BtnPlan4 { get; set; }


        [Outlet]
        UIKit.UILabel DateLabel { get; set; }


        [Outlet]
        public PapeApp.iOS.Views.CircleButton TodayMark { get; set; }

        void ReleaseDesignerOutlets ()
        {
        }
    }
}
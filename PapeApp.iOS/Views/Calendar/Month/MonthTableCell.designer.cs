// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace PapeApp.iOS
{
    [Register ("MonthTableCell")]
    partial class MonthTableCell
    {
        [Outlet]
        PapeApp.iOS.Views.CircleButton ColorMark { get; set; }


        [Outlet]
        UIKit.UILabel TimeLabel { get; set; }


        [Outlet]
        UIKit.UILabel TitleLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
        }
    }
}
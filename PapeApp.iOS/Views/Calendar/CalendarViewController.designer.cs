// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace PapeApp.iOS
{
    [Register ("CalendarViewController")]
    partial class CalendarViewController
    {
        [Outlet]
        UIKit.UIButton BackButton { get; set; }


        [Outlet]
        UIKit.UISegmentedControl CalendarSegmentedControl { get; set; }


        [Outlet]
        UIKit.UIView ContainerView { get; set; }


        [Outlet]
        UIKit.UILabel DateHeaderLbl { get; set; }


        [Outlet]
        UIKit.UIImageView ImageView { get; set; }

        void ReleaseDesignerOutlets ()
        {
        }
    }
}
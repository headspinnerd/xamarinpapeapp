﻿using System;
using System.ComponentModel;
using System.Reactive.Linq;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using GalaSoft.MvvmLight.Ioc;
using PapeApp.iOS.Views;
using PapeApp.Services;
using PapeApp.ViewModels;
using PapeApp.Utility;
using UIKit;

namespace PapeApp.iOS
{
    public partial class CalendarViewController : BaseViewController<CalendarViewModel>
    {
        private CalendarContainerView containerViewController;

        private int SelectedId;

        public IGoogleCalendarService GoogleCalendarService
            => SimpleIoc.Default.GetInstance<IGoogleCalendarService>();

        public CalendarViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            BackButton.TouchUpInside += (sender, e) 
                => ViewModel.BackCommand.Execute();
            

            //select the First view
            CalendarSegmentedControl.SelectedSegment = 0;
            PresentContainerView(CalendarSegmentedControl.SelectedSegment);

            CalendarSegmentedControl.ValueChanged += (sender, e) =>
            {
                var selectedSegmentId = ((UISegmentedControl)sender).SelectedSegment;
                PresentContainerView(selectedSegmentId);
            };

            GoogleCalendarService.CurrentDate.Subscribe(date =>
                DateHeaderLbl.Text = date.ConvertToDateHeaderLbl(SelectedId));
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            ImageView.Layer.CornerRadius = 20;
            ImageView.Layer.MasksToBounds = true;
            containerViewController.View.Layer.CornerRadius = 20;
            containerViewController.View.Layer.MasksToBounds = true;
            containerViewController.View.Layer.MaskedCorners =
                CoreAnimation.CACornerMask.MaxXMaxYCorner
                | CoreAnimation.CACornerMask.MinXMaxYCorner;
        }

        async void PresentContainerView(nint selectedId)
        {
            //we need some synchronisation because the new view controller
            //is animated in. Disable the switch until the animation is complete
            CalendarSegmentedControl.Enabled = false;
            switch (selectedId)
            {
                case 0:
                    await containerViewController.PresentCalendarMonthViewAsync();
                    break;
                case 1:
                    await containerViewController.PresentCalendarListViewAsync();
                    break;
                case 2:
                    await containerViewController.PresentCalendarDayViewAsync();
                    break;
                case 3:
                    await containerViewController.PresentCalendarWeekViewAsync();
                    break;
            }
            SelectedId = (int)selectedId;
            CalendarSegmentedControl.Enabled = true;
        }

        public override void DidReceiveMemoryWarning() 
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            if (segue.Identifier == "embededContainer")
            {
                containerViewController =
                segue.DestinationViewController as CalendarContainerView;
            }
        }
    }
}


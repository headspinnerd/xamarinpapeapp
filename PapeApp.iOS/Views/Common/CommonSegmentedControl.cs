// This file has been autogenerated from a class added in the UI designer.

using System;

using Foundation;
using UIKit;

namespace PapeApp.iOS.Views
{
	public partial class CommonSegmentedControl : UISegmentedControl
	{
		public CommonSegmentedControl (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            SetTitleTextAttributes(new UITextAttributes 
                { Font = UIFont.FromName("PenguinAttack", 15) },
                UIControlState.Normal);
        }
    }
}

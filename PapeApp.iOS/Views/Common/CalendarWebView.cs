﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;
using WebKit;

namespace PapeApp.iOS.Views.Common
{
    //public class CalendarSummaryMemoCell : UITableViewCell
    //{
    //    /// <summary>
    //    /// 高さ変更時に呼び出されるイベント
    //    /// </summary>
    //    public event EventHandler UpdateCellHeight;

    //    public IAppCoopApi AppCoopApi { get; set; }

    //    public CalendarSummaryMemoCell(IntPtr handle) : base(handle)
    //    {
    //    }

    //    public override void AwakeFromNib()
    //    {
    //        base.AwakeFromNib();
    //        Title.Text = Strings.CalendarSummaryView_Label_Memo;
    //        Memo.WebViewDelegate.LoadFinished += (sender, e) =>
    //        {
    //            InvokeOnMainThread(() =>
    //            {
    //                MemoHeight.Constant = (nfloat)Math.Max(Memo.Bounds.Height,
    //                    Memo.ScrollHeight);
    //                UpdateCellHeight?.Invoke(this, EventArgs.Empty);
    //            });
    //        };
    //    }

    //    public void SetData(string memo)
    //    {
    //        Memo.AppCoopApi = AppCoopApi;

    //        if (memo == null) memo = string.Empty;
    //        Memo.DisplayText(memo);
    //    }

    //    public override void SetHighlighted(bool highlighted, bool animated)
    //    {
    //        // セル押下時のhighlight表示を無効にする
    //    }
    //}

    public class CalendarWebView: UIView
    {
        public double ScrollHeight { get; set; }

        public WKWebView WebView { get; set; }
        protected WKWebViewConfiguration Config { get; set; }
        public WebViewDelegate WebViewDelegate { get; set; }

        public CalendarWebView(IntPtr handle): base (handle)
        {
            Initialize();
            SetUp();
        }

        [Export("initWithCoder:")]
        public CalendarWebView(NSCoder coder): base(coder)
        {
            Initialize();
            SetUp();
        }

        public override UIColor BackgroundColor
        {
            get => base.BackgroundColor;
            set
            {
                base.BackgroundColor = value;
                if(WebView != null)
                {
                    WebView.BackgroundColor = value;
                }
            }
        }

        private void Initialize()
        {
            Config = new WKWebViewConfiguration();
            Config.DataDetectorTypes = WKDataDetectorTypes.All;
            WebView = new WKWebView(Frame, Config);
            WebView.Frame = new CGRect(0, 0, Frame.Width, Frame.Height);
            //WebView.UIDelegate = new UIDelegate(this);
            WebView.AutoresizingMask = UIViewAutoresizing.All;
            WebViewDelegate = new WebViewDelegate(this);
            WebView.NavigationDelegate = WebViewDelegate;
            AddSubview(WebView);
        }

        private const string HtmlFormat = @"<html>
                                            <head>
                                            <style type=""text/css"">
                                            body {{font-family: ""{0}""; font-style: normal; 
                                            font-weight: lighter; font-size: {1}; margin: 0; 
                                            padding: 0; line-height: 1.3; }}
                                            a:link {{color:#007AFF; text-decoration: none;}}
                                            </style>
                                            <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
                                            </head>
                                            <body>{2}</body>
                                            </html>";
        private bool isDisplayed;
        public UIFont Font = UIFont.SystemFontOfSize(12);

        public void DisplayText(string textString)
        {
            if(textString == null)
            {
                isDisplayed = true;
                WebView.Hidden = true;
                return;
            }
            var text = string.Format(HtmlFormat, Font.FamilyName,
                Font.PointSize, textString.Replace("\n", "<br>"));
            if (!isDisplayed)
            {
                WebView.LoadHtmlString(text, null);
                isDisplayed = true;
            }
        }

        private void SetUp()
        {
            Opaque = false;
            BackgroundColor = UIColor.Clear;
            WebView.ScrollView.ScrollEnabled = false;
            WebView.ScrollView.Bounces = false;
            WebView.ScrollView.Layer.MasksToBounds = false;
        }
    }

    public class UIDelegate : WKUIDelegate
    {
        private CalendarWebView _calendarWebView;

        public UIDelegate(CalendarWebView calendarWebView)
        {
            _calendarWebView = calendarWebView;
        }

        public override bool ShouldPreviewElement(WKWebView webView, WKPreviewElementInfo elementInfo)
        {
            var url = elementInfo.LinkUrl;
            var scheme = url.Scheme?.ToLower();
            //return _calendarWebView.xxxx.ShouldInterceptPreview(scheme);
            return true;
        }

        public override UIViewController GetPreviewingViewController(WKWebView webView, WKPreviewElementInfo elementInfo, IWKPreviewActionItem[] previewActions)
        {
            return null;
        }

        //public override void CommitPreviewingViewController(WKWebView webView, UIViewController previewingViewController)
        //{
        //    if (previewingViewController is WebPreviewViewController webPreview)
        //        _calendarWebView.WebView.
        //}
    }

    //internal class WebPreviewViewController: UIViewController
    //{
    //    public readonly NSUrl LinkUrl;
    //    private readonly IWKPreviewActionItem[] WebViewPreviewActionItems;
    //    private CalendarWebView _calendarWebView;

    //    public WebPreviewViewController(NSUrl linkUrl,
    //        IWKPreviewActionItem[] webViewPreviewActionItems, CalendarWebView calendarWebView)
    //    {
    //        LinkUrl = linkUrl;
    //        WebViewPreviewActionItems = webViewPreviewActionItems;
    //        _calendarWebView = calendarWebView;
    //    }

    //    public WebPreviewViewController(string nibName, NSBundle bundle): base(nibName, bundle)
    //    {
    //    }

    //    public override IUIPreviewActionItem[] PreviewActionItems => WebViewPreviewActionItems;

    //    public override void ViewDidLoad()
    //    {
    //        base.ViewDidLoad();

    //        if (LinkUrl is NSUrl url)
    //        {
    //            var web = new WKWebView(View.Bounds, new WKWebViewConfiguration());
    //            web.LoadRequest(new NSUrlRequest());
    //        }
    //    }
    //}

    public class WebViewDelegate: WKNavigationDelegate
    {
        public event EventHandler LoadFinished;
        private CalendarWebView _calendarWebView;

        public WebViewDelegate(CalendarWebView calendarWebView)
        {
            _calendarWebView = calendarWebView;
        }

        private static readonly List<string> AllowSchemes = new List<string>
        {
            "http", "https", "ftp", "mailto", "tel"
        };

        public override void DecidePolicy(WKWebView webView,
            WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
        {
            var url = navigationAction.Request.Url;
            if (navigationAction.NavigationType == WKNavigationType.LinkActivated)
            {
                var scheme = url.Scheme?.ToLower();
                if (AllowSchemes.Contains(scheme?.ToLower()))
                {
                    decisionHandler?.Invoke(WKNavigationActionPolicy.Allow);
                }
                else
                {
                    decisionHandler?.Invoke(WKNavigationActionPolicy.Cancel);
                }
            }
            else
            {
                decisionHandler?.Invoke(WKNavigationActionPolicy.Allow);
            }
        }

        public override void DidFinishNavigation(WKWebView webView,
            WKNavigation navigation)
        {
            webView.EvaluateJavaScript("document.readyState", (r, e) =>
            {
                webView.EvaluateJavaScript("document.body.scrollHeight", (re, er) =>
                {
                    if (re is NSNumber result)
                    {
                        _calendarWebView.ScrollHeight = result.DoubleValue;
                    }
                    LoadFinished?.Invoke(this, EventArgs.Empty);
                });
            });
        }
    }
}

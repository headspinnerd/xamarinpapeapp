// This file has been autogenerated from a class added in the UI designer.

using System;

using Foundation;
using UIKit;

namespace PapeApp.iOS.Views
{
	public partial class CommonLabel : UILabel
	{
		public CommonLabel (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            Font = UIFont.FromName("PenguinAttack", Font.PointSize);
        }
    }
}

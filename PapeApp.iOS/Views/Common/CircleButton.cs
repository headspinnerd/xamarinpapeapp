// This file has been autogenerated from a class added in the UI designer.

using System;
using System.ComponentModel;
using Foundation;
using UIKit;

namespace PapeApp.iOS.Views
{
    public partial class CircleButton : UIButton
	{
        private nfloat cornerRadius = 0;

        [Export("CornerRadius"), Browsable(true)]
        public nfloat CornerRadius {
            get 
            {
                return cornerRadius == 0 
                    ? this.Frame.Width/2 : cornerRadius;
            }
            set 
            {
                cornerRadius = value;
            }
        }
        public CircleButton (IntPtr handle) : base (handle)
		{
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            SetupView();
        }

        private void SetupView() 
        {
            Layer.CornerRadius = CornerRadius;
        }
    }
    //    override func prepareForInterfaceBuilder()
    //{
    //    setupView()
    //    }
}

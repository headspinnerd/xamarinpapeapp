﻿using System;
using System.Threading.Tasks;

namespace PapeApp.iOS.Views.Navigation
{
    public interface ITransitioningViewController
    {
        TaskCompletionSource<bool> ViewChanging { get; set; }
    }
}

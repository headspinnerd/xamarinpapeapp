﻿using System;
using System.ComponentModel;
using System.Reactive.Linq;
using Foundation;
using UIKit;
using GalaSoft.MvvmLight.Helpers;
using PapeApp.ViewModels;

namespace PapeApp.iOS
{
    public partial class HomeViewController : BaseViewController<HomeViewModel>
    {
        public HomeViewController(IntPtr handle) : base(handle)
        {
        }
        
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.SetBinding(() => ViewModel.TestLabelString.Value).WhenSourceChanges(() =>
            {
                //TestLabel.Text = ViewModel.TestLabelString.Value;
            });

            CalendarBtn.TouchUpInside += (sender, e) => ViewModel.CalendarCommand.Execute();
            //CalendarBtn.SetCommand(ViewModel.CalendarCommand);
            //Observable.FromEventPattern(CalendarBtn, "TouchUpInside")
            //.SetCommand(ViewModel.CalendarCommand);

            //TODO: need to implement login screen
            var preference = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<PapeApp.Services.IPreferenceService>();
            var appSettings = GalaSoft.MvvmLight.Ioc.SimpleIoc.Default.GetInstance<PapeApp.Models.Settings.IAppSettings>();
            if (!preference.ContainValue(Models.UserDefaultsKey.SpouseMailAddress))
            {
                var chooseUserAlert = UIAlertController.Create("Choose one", "Who are you?", UIAlertControllerStyle.Alert);
                chooseUserAlert.AddAction(UIAlertAction.Create("Koki", UIAlertActionStyle.Default,
                    _ =>
                    {
                        appSettings.SpouseMailAddress = "miyumiyu.h.117@gmail.com";
                        appSettings.SpouseName = "Miyu";
                    })
                );
                chooseUserAlert.AddAction(UIAlertAction.Create("Miyu", UIAlertActionStyle.Default,
                    _ =>
                    {
                        appSettings.SpouseMailAddress = "kouairchair@gmail.com";
                        appSettings.SpouseName = "Koki";
                    })
                );

                PresentViewController(chooseUserAlert, true, null);
            }
        }

        public override void DidReceiveMemoryWarning() 
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}


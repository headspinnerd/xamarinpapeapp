﻿using System;
using UIKit;

namespace PapeApp.iOS
{
    // TODO: Couldn't implement this class on Main.cs
    public class AppMain: UIApplication
    {
        public static UIViewController TopViewController(UIViewController _baseView = null)
        {
            var baseView = _baseView ?? SharedApplication.KeyWindow.RootViewController; 
            if (baseView is UINavigationController nav)
            {
                return TopViewController(nav.VisibleViewController);
            }
            if (baseView is UITabBarController tab)
            {
                var moreNavigationContoroller = tab.MoreNavigationController;
                if (moreNavigationContoroller.TopViewController is UIViewController top
                    && top.View.Window != null)
                {
                    return TopViewController(top);
                }
                else if (tab.SelectedViewController is UIViewController selected)
                {
                    return TopViewController(selected);
                }
            }
            if (baseView.PresentedViewController is UIViewController presented)
            {
                return TopViewController(presented);
            }
            return baseView;
        }
    }
}

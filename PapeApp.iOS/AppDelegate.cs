﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using Microsoft.Practices.ServiceLocation;
using UIKit;
using GalaSoft.MvvmLight.Views;
using GalaSoft.MvvmLight.Ioc;
using NLog;
using PapeApp.ViewModels;
using Google.SignIn;
using PapeApp.Services;
using PapeApp.iOS.Services;
using PapeApp.Models.Settings;
using PapeApp.Utility;

namespace PapeApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            SetupViewModel();
            SetupNavigationService();
            SetupServices();

            return true;
        }

        private void SetupNavigationService()
        {
            //// Initialize and register the Navigation Service
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            var nav = new PapeApp.iOS.Views.Navigation.NavigationService();

            nav.Initialize(Window.RootViewController as UINavigationController);
            nav.Configure(HomeViewModel.ViewModelKey);
            nav.Configure(CalendarViewModel.ViewModelKey);
            nav.Configure(CalendarContainerViewModel.ViewModelKey);
            nav.Configure(CalendarMonthViewModel.ViewModelKey);
            nav.Configure(CalendarListViewModel.ViewModelKey);
            nav.Configure(CalendarDayViewModel.ViewModelKey);
            nav.Configure(CalendarWeekViewModel.ViewModelKey);
            nav.Configure(CalendarEditViewModel.ViewModelKey);
            SimpleIoc.Default.Register<IExtendedNavigationService>(() => nav);
        }

        private void SetupServices()
        {
            ILogger logger = LogManager.GetCurrentClassLogger();
            SimpleIoc.Default.Register<ILogger>(() => logger);
            SimpleIoc.Default.Register<IGoogleSignInService>(() => new GoogleSignInService());
            SimpleIoc.Default.Register<IGoogleCalendarService>(() => new GoogleCalendarService());
            SimpleIoc.Default.Register<IPreferenceService>(() => new PreferenceService());
            SimpleIoc.Default.Register<IDialogService>(() => new GalaSoft.MvvmLight.Views.DialogService());
            SimpleIoc.Default.Register<IAppSettings>(() => new AppSettings());
        }

        private void SetupViewModel()
        {
            SimpleIoc.Default.Register<BaseViewModel>();
            SimpleIoc.Default.Register<HomeViewModel>();
            SimpleIoc.Default.Register<CalendarViewModel>();
            SimpleIoc.Default.Register<CalendarContainerViewModel>();
            SimpleIoc.Default.Register<CalendarMonthViewModel>();
            SimpleIoc.Default.Register<CalendarListViewModel>();
            SimpleIoc.Default.Register<CalendarDayViewModel>();
            SimpleIoc.Default.Register<CalendarWeekViewModel>();
            SimpleIoc.Default.Register<CalendarEditViewModel>();
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            var openUrlOptions = new UIApplicationOpenUrlOptions(options);
            return SignIn.SharedInstance.HandleUrl(url, openUrlOptions.SourceApplication, openUrlOptions.Annotation);
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
    }

}

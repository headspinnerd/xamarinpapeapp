﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using NLog;
using PapeApp.Services;
using PapeApp.ViewModels;
using UIKit;

namespace PapeApp.iOS
{
    public class BaseViewController<ViewModelBase> : UIViewController where ViewModelBase : BaseViewModel
    {
        protected ViewModelBase ViewModel { get; set; }

        protected List<IDisposable> DisposablePools = new List<IDisposable>();

        public BaseViewController(IntPtr handle) : base(handle)
        {
            if (SimpleIoc.Default.IsRegistered<ViewModelBase>())
            {
                ViewModel = SimpleIoc.Default.GetInstance<ViewModelBase>();
            }
            else
            {
                Log.Instance?.Warn($"{typeof(ViewModelBase)} is not registered");
            }
        }
       
        protected object Parameter
        {
            get
            {
                return ((Views.Navigation.NavigationService)SimpleIoc.Default.GetInstance<IExtendedNavigationService>())
                    .GetAndRemoveParameter(this);
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            ViewModel?.OnLoad(Parameter);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ViewModel?.OnAppear();
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            ViewModel?.OnDisappear();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                DisposablePools?.ForEach(d => d?.Dispose());
            base.Dispose(disposing);
        }
    }
}

